package com.plaoaltonetworks.ccsconsumer;

import com.plaoaltonetworks.ccsconsumer.service.SQSMessageDispatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CcsconsumerApplication   implements CommandLineRunner {
  @Autowired
	SQSMessageDispatch service;
	public static void main(String[] args) {
		SpringApplication.run(CcsconsumerApplication.class, args);
	}
		public void run(String... args) {
		   service.polling();
	 }

}
