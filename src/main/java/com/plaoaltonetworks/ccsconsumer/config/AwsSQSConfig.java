package com.plaoaltonetworks.ccsconsumer.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

@Configuration
@Slf4j
public class AwsSQSConfig {
 public static Region region= Region.US_EAST_1;
  @Bean
  public AmazonSQS amazonExtendSQS() {

    AmazonSQS sqsClient = AmazonSQSClientBuilder.standard()
        .withCredentials(credentialsProvider())
        .withRegion(region.toString())
        .build();
    return  sqsClient;
  }
 @Bean
  public DynamoDbClient dynamoDb(){
    DynamoDbClient ddb = DynamoDbClient.builder()
        .region(region)
        .build();

    return ddb;
  }

  @Bean
  public AWSCredentialsProvider credentialsProvider() {
    return new DefaultAWSCredentialsProviderChain();
  }

}
