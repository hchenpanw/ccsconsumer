package com.plaoaltonetworks.ccsconsumer.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecordChangeDetail {

  private String action;

  private Long recordChangeTs;

  private String type;

  private String recordId;

  private String prismaId;

  private boolean isTruncated;

  private String callbackURL;


}