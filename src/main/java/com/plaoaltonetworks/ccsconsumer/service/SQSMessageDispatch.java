package com.plaoaltonetworks.ccsconsumer.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.plaoaltonetworks.ccsconsumer.model.RecordChangeDetail;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;

@Service
public class SQSMessageDispatch {
  private static String sqsQueueUrl = "https://sqs.us-east-1.amazonaws.com/661959982153/ccs-apptwisqa-RESOURCE-PC-CNS-1dc7abf7-2680-4498-8605-ebd03d162ca8";
  //private static String sqsQueueUrl = "https://sqs.us-east-1.amazonaws"
    //  +".com/661959982153/ccs-appavenqa-RESOURCE-PC-PLATFORMAUTO-3ce79f90-3baa-4db9-b17a-1e4bb8eeb015";
  private static String tableName = "ccs_resource_new";
  private static String pkey = "recordId";
  private static String recordKey = "sortKey";
  private static String payload = "payload";
  @Autowired
  private AmazonSQS amazonExtendSQS;
  @Autowired
  private DynamoDbClient dynamoDbClient;

  private ObjectMapper mapper = new ObjectMapper(); // create once, reuse

  public void polling(){
    while(true) {
      ReceiveMessageResult messageResult = amazonExtendSQS.receiveMessage(sqsQueueUrl);
      messageResult.getMessages().forEach(m ->  store2DB(m));
    }

  }

  private String getSortKey( RecordChangeDetail evt ){
    return "" + evt.getRecordChangeTs() +"#" + evt.getAction();
  }

  private String getPrimaryKey( RecordChangeDetail evt ){
    return "" + evt.getPrismaId() + "#" + evt.getRecordId();
  }

  private void store2DB(Message message)  {
    System.out.println("==> store message");
    RecordChangeDetail evt = null;
    try {
       evt = mapper.readValue(message.getBody(), RecordChangeDetail.class);
    }catch(JsonProcessingException ex){
      System.out.println("fail to parse JSON body:" + message.getBody());
      System.exit(1);
    }

    HashMap<String,AttributeValue> itemValues = new HashMap<String, AttributeValue>();
    itemValues.put(pkey, AttributeValue.builder().s(getPrimaryKey(evt)).build());
    itemValues.put(recordKey, AttributeValue.builder().s(getSortKey(evt)).build());
    itemValues.put(payload, AttributeValue.builder().s(message.getBody()).build());
    PutItemRequest request = PutItemRequest.builder()
        .tableName(tableName)
        .item(itemValues)
        .build();
    try {
      dynamoDbClient.putItem(request);
      System.out.println(tableName +" was successfully updated");

    } catch (ResourceNotFoundException e) {
      System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
      System.err.println("Be sure that it exists and that you've typed its name correctly!");
      System.exit(1);
    } catch (DynamoDbException e) {
      System.err.println(e.getMessage());
      System.exit(1);
    }
  }

}
